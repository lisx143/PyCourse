import requests
from pyquery import PyQuery as pq


def getInfo(url, code="utf-8"):
    try:
        r = requests.get(url)
        r.raise_for_status()
        r.encoding = code
        return r.text
    except:
        return ""

baiduurl = 'http://www.baidu.com/s?wd=李双喜'
doc = pq(getInfo(baiduurl))
mlist = doc('#content_left h3.t a').items()
i = 0
for li in mlist:
    i = i + 1
    print('标题：' + li.text())
    print('链接：' + li.attr('href'))